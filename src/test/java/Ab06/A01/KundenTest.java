package Ab06.A01;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KundenTest {

    Kunden kunden;

    @BeforeEach
    void setUp() {
        kunden = new Kunden("Hari", "Aaron");
    }

    @Test
    void getName() {
        assertEquals("Aaron Hari", kunden.getName());
    }

    @Test
    void setName() {
        kunden.setName("Wenger", "Aaron");
        assertEquals("Aaron Wenger", kunden.getName());
    }
}