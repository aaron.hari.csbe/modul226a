package Ab06.A01;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KontoTest {

    Konto konto;

    @BeforeEach
    void setUp() {
        konto = new Konto(0.005);
            }

    @Test
    void einzahlen() {
        konto.einzahlen(19);
        assertEquals(19, konto.getSaldo());
    }

    @Test
    void verzinsen() {
        konto.einzahlen(100);
        konto.verzinsen(365);
        assertEquals(100.5, konto.getSaldo());
    }

    @Test
    void getSaldo() {
        assertEquals(0, konto.getSaldo());
    }
}