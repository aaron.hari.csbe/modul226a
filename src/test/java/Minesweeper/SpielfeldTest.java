package Minesweeper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpielfeldTest {

    Spielfeld spielfeld = new Spielfeld();

    @BeforeEach
    void setUp() {
        spielfeld.HOEHE = 16;
        spielfeld.BREITE = 16;
        spielfeld.ANZAHL_MINEN = 40;
    }

    @Test
    void zufaelligMinenVerteilen() {
        spielfeld.spielfeldErstellen();
        spielfeld.zufaelligMinenVerteilen();
        int zahler = 0;
        for (int i = 0; i < spielfeld.BREITE; i++) {
            for (int j = 0; j < spielfeld.HOEHE; j++) {
                if (spielfeld.spielfeld[j][i].istMine()) {
                    zahler++;
                }
            }
        }
        assertEquals(spielfeld.ANZAHL_MINEN, zahler);
    }

    @Test
    void zufaelligeZahl() {
        int kleinsteMoeglilcheZahl = 24;
        int groessteMoeglicheZahl = 100;
        int testErgebniss;
        int zufaelligeZahl = spielfeld.zufaelligeZahl(kleinsteMoeglilcheZahl, groessteMoeglicheZahl);
        if (zufaelligeZahl >= kleinsteMoeglilcheZahl && zufaelligeZahl <= groessteMoeglicheZahl) {
            testErgebniss = zufaelligeZahl;
        } else {
            testErgebniss = zufaelligeZahl + 1;
        }
        assertEquals(testErgebniss, zufaelligeZahl);
    }

    @Test
    void spielfeldErstellen() {
        spielfeld.spielfeldErstellen();
        try {
            for (int i = 0; i < spielfeld.BREITE; i++) {
                for (int j = 0; j < spielfeld.HOEHE; j++) {
                    spielfeld.spielfeld[i][j].istMine();
                }
            }
            assertFalse(false);
        } catch (Exception e) {
            assertFalse(true);
        }

    }

    @Test
    void anzahlMinenInUmkreisBerechnen() {
        boolean testErgebniss = true;
        spielfeld.spielfeldErstellen();
        spielfeld.zufaelligMinenVerteilen();
        spielfeld.anzahlMinenInUmkreisBerechnen();
        for (int i = 0; i < spielfeld.HOEHE; i++) {
            for (int j = 0; j < spielfeld.BREITE; j++) {
                if (spielfeld.spielfeld[i][j].ANZAHL_MINEN_IN_UMKREIS != 0){
                    testErgebniss = false;
                }
            }
        }

        System.out.println(Farben.Gruen.erhalteFarbe() + "Hallo");
       assertFalse(testErgebniss);
    }

    @Test
    void spielfeldAnzeigen() {
        spielfeld.spielfeldErstellen();
        spielfeld.zufaelligMinenVerteilen();
        spielfeld.anzahlMinenInUmkreisBerechnen();
        spielfeld.spielfeldAnzeigen();
    }

    @Test
    void testSpielfeldAnzeigen() {
    }

    @Test
    void anzahlZiffern() {
        int anzahlZiffern = spielfeld.anzahlZiffern(10);
        assertEquals(2, anzahlZiffern);
    }
}