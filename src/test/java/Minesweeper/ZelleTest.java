package Minesweeper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZelleTest {
    Zelle zelle = new Zelle();

    @BeforeEach
    void setUp() {

    }

    @Test
    void istMine() {
        assertEquals(false, zelle.istMine());
    }

    @Test
    void istVerdeckt() {
        assertEquals(true, zelle.istVerdeckt());
    }

    @Test
    void istMarkiert() {
        assertEquals(false, zelle.istMarkiert());
    }

    @Test
    void anzahlMinenInUmkreis() {
        assertEquals(0, zelle.ANZAHL_MINEN_IN_UMKREIS);
    }

    @Test
    void aufdecken() {
        zelle.aufdecken();
        assertEquals(false, zelle.istVerdeckt());
    }

    @Test
    void markieren() {
        zelle.markieren();
        assertEquals(true, zelle.istMarkiert());
    }

    @Test
    void markierungAufheben() {
        zelle.markierungAufheben();
        assertEquals(false, zelle.istMarkiert());
    }
}