package Ab08;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class a01Test {

    private a01 person;

    @BeforeEach
    void setUp() {
        person = new a01("Hari", "Aaron");
    }

    @Test
    void getName() {
        assertEquals("Hari", person.getName());
    }
}