package Ab05;

public class a01 {
    public static void main(String[] args) {
        System.out.println(calc(5, 10, 1));
        System.out.println(calc(5, 10, 2));
        System.out.println(calc(5, 10, 3));
        System.out.println(calc(5, 10, 4));
        System.out.println(calc(5, 10, 5));
    }

    public static double calc(int operand1, int operand2, int operand3){
        switch (operand3){
            case 1:
                return operand1 + operand2;
            case 2:
                return operand1 - operand2;
            case 3:
                return operand1 * operand2;
            case 4:
                return operand1 / operand2;
            case 5:
                return operand1 % operand2;
            default:
                return 0;
        }
    }
}
