package AB04;

import java.util.Random;

public class A01b {
    public static void main(String[] args) {
        /*
        Sie müssten an sechs Tagen,
        an drei verschiedenen Orten
        und zu vier unterschiedlichen Zeiten
        jeweils die Temperatur erfassen.
         */


        int[][][] liste = new int[6][3][4];
        Random generator = new Random();

        for (int i = 0; i < 6; i++){
            //Tag
            System.out.println("Tag " + (i + 1) + " |1.|2.|3.|4.|");
            for (int j = 0; j < 3; j++){
                //Ort
                System.out.print("Ort " + (j + 1) + ": ");
                int groessteZahl = 0;
                for (int y = 0; y < 4; y++){
                    //Zeitpunkt / Temperatur
                    liste[i][j][y] = generator.nextInt(11) + 20;
                    System.out.print(liste[i][j][y] + " ");
                    if (liste[i][j][y] > groessteZahl){
                        groessteZahl = liste[i][j][y];
                    }
                }
                System.out.print(" --> Die höchste Temperatur ist: " + groessteZahl + "°");
                System.out.println(" ");
            }
            System.out.println(" ");
        }
    }
}
