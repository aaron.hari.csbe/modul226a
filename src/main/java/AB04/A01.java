package AB04;

import java.lang.reflect.Array;
import java.util.Random;

public class A01 {

    private final static int LISTEN_LAENGE = 30;
    private final static int MAX_ZAHL = 30;

    public static void main(String[] args) {
        int[] zahlenListe = new int[LISTEN_LAENGE];
        int[] zahlenListeSortiert = new int[LISTEN_LAENGE];
        Random zahlGenerator = new Random();

        for (int i = 0; i < LISTEN_LAENGE; i++) {
            zahlenListe[i] = zahlGenerator.nextInt(MAX_ZAHL);
        }
        listeAusgeben(zahlenListe);
        zahlenListeSortiert = bubbleSort(zahlenListe.clone());
        listeAusgeben(zahlenListeSortiert);

        System.out.println(listenVergleichen(zahlenListe, zahlenListeSortiert));
    }

    public static int listenVergleichen(int[] liste, int[] listeSortiert){
        int anzahlGroessereZahlen = 0;
        for (int i = 0; i < LISTEN_LAENGE; i++){
            if (liste[i] > listeSortiert[i]){
                anzahlGroessereZahlen++;
            }
        }
        return anzahlGroessereZahlen;
    }

    public static int[] bubbleSort(int[] liste) {
        int laenge = liste.length;
        int temporaer = 0;
        for (int i = 0; i < laenge; i++) {
            for (int j = 1; j < (laenge - i); j++) {
                if (liste[j - 1] > liste[j]) {
                    temporaer = liste[j - 1];
                    liste[j - 1] = liste[j];
                    liste[j] = temporaer;
                }

            }
        }
        return liste;
    }

    public static void listeAusgeben(int[] liste) {
        for (int o : liste) {
            System.out.print(o + " ");
        }
        System.out.println("");
    }
}
