package Ab06.A01;

public class testKonto {

    public static void main(String[] args) {
        Konto testKontoEins = new Konto(3);
        Konto testKontoZwei = new Konto(4);

        testKontoEins.inhaber.setName("Hari", "Aaron");
        testKontoZwei.inhaber.setName("Senften", "Daniel");

        testKontoEins.einzahlen(500.75);
        testKontoEins.verzinsen(365);

        testKontoZwei.einzahlen(720.50);
        testKontoZwei.verzinsen(720);

        testKontoZwei.getSaldo();
        testKontoZwei.einzahlen(340.34);
        testKontoZwei.getSaldo();

    }
}
