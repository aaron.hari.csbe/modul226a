package Ab06.A01;



public class Konto {

    /**
     * @zinssatz, Ein 100% Zinssatz espricht 0.1, ein 0.5% Zinssatz espricht 0.005. Der Zinssatz in Prozent(%) wird mal 0.001 gerechnet.
     */

    private double saldo = 0;
    private double zinssatz;

    Kunden inhaber = new Kunden();

    public Konto(double zinssatz){
        this.zinssatz = zinssatz;
    }

    public void einzahlen(double betrag){
        saldo += betrag;
    }

    public void verzinsen(int tage){
        if (saldo < 50000){
            saldo += saldo * zinssatz * tage / 365;
        }else if (saldo < 500000){
            saldo += saldo * (zinssatz / 2) * tage / 365;
        }else{
            saldo += saldo * tage / 365;
        }
    }

    public double getSaldo(){
        inhaber.getName();
        System.out.println("Saldo: " + saldo);
        return saldo;
    }

}
