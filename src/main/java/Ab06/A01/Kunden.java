package Ab06.A01;

public class Kunden {

    private String name;
    private String vorname;
    private int kundennummer;

    /**
     * Dies ist ein null Konstruktor
     */
    public Kunden(){
    }

    public Kunden(String name, String vorname){
        this.name = name;
        this.vorname = vorname;
    }

    public String getName(){
        System.out.println(vorname + " " + name);
        return vorname + " " + name;
    }

    public void setName(String name, String vorname){
        this.name = name;
        this.vorname = vorname;
    }
}
