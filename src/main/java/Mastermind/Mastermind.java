package Mastermind;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Mastermind {

    public static void main(String[] args) {
        System.out.println("Geben Sie einen versuchs Code mit vier Buchstaben aus der Menge {r, g, b, y, s, w} ein: ");

        System.out.println("Spiel beendet. Der Geheimcode war Anzahl der versuche: " + spielSchleife(geheimCode()));

    }

    public static int spielSchleife(char[] geheimCode){
        boolean spielStatus = true;
        int[] anzahlSchwarzWeiss;
        var versuche = 0;

        while (spielStatus){
            versuche++;
            anzahlSchwarzWeiss = codeAbgleichen(geheimCode, benutzerEingabe());
            if (anzahlSchwarzWeiss[0] == 4){
                System.out.println("Du hast den Geheim Code erraten!!");
                spielStatus = false;
            }else{
                System.out.println("Schwarz: " + anzahlSchwarzWeiss[0] + "\r\nWeiss: " + anzahlSchwarzWeiss[1]);
            }
        }
        return versuche;
    }

    public static int[] codeAbgleichen(char[] geheimCode, char[] inputCode){
        int[] anzahlSchwarzWeiss = new int[2];
        ArrayList<Integer> blackList = new ArrayList<Integer>();

        for (int i = 0; i < geheimCode.length; i++){
            var schwarz = 0;
            var weiss = 0;

            for (int y = 0; y < geheimCode.length; y++){
                for (int j : blackList) {
                    if (y == j){
                        y++;
                    }
                }
                if (geheimCode[i] == inputCode[y]){
                    if (i == y){
                        schwarz++;
                        //inputCode[y] kommt auf eine Blacklist und darf nicht mehr überprüft werden.
                        blackList.add(y);
                    }else{
                        weiss++;
                    }
                }
            }
            if (schwarz == 1){
                anzahlSchwarzWeiss[0] += 1;
            }else if (weiss > 0){
                anzahlSchwarzWeiss[1] += 1;
            }
        }
        return anzahlSchwarzWeiss;
    }

    public static char[] benutzerEingabe(){
        Scanner input = new Scanner(System.in);
        String inputString = "empty";
        char[] inputCode = new char[4];
        var nichtValide = true;
        //inputString = input.next();

        while(nichtValide){
            try {
                inputString = input.next();
            }catch (Exception e){
            }
            nichtValide = inputUeberpruefung(inputString);
        }

        for (int i = 0; i < 4; i++){
            inputCode[i] = inputString.charAt(i);
        }
        return inputCode;
    }

    public static char[] geheimCode() {
        char[] geheimCode = new char[4];
        Random zufaelligeZahlGenerator = new Random();
        int zufaelligeZahl;
        char[] farben = {'r', 'g', 'b', 'y', 's', 'w'};

        for (int i = 0; i < geheimCode.length; i++) {
            zufaelligeZahl = zufaelligeZahlGenerator.nextInt(6);
            geheimCode[i] = farben[zufaelligeZahl];
        }
        System.out.println(geheimCode);
        return geheimCode;
    }

    public static boolean inputUeberpruefung(String inputString){

        boolean nichtValide = true;

        if (inputString.length() != 4){
            return true;
        }

        for (int i = 0; i < 4; i++){
            char buchstaben = inputString.charAt(i);

            nichtValide = switch(buchstaben){
                case 'r', 'g', 'b', 'y', 's', 'w' -> false;
                default -> true;
            };
        }
        return nichtValide;
    }
}
