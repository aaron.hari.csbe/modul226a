package Ab03;

import java.util.Random;

public class A02 {
    public static void main(String[] args) {
        Random generator = new Random();
        var randomZahl = 0;

        for (int i = 1; i < 11; i++){
            randomZahl = generator.nextInt(4);
            System.out.print("Zeile " + i + ": Zufallszahl: " + (randomZahl + 1) + " ");
            System.out.print("X");

            float abstand = (randomZahl + 1);
            abstand = 10 / abstand;
            int abstandInt = Math.round(abstand) - 1;
            StringBuilder ausgabe = new StringBuilder();

            while (ausgabe.length() < 9) {
                for (int y = 0; y < randomZahl + 1; y++) {
                    for (int j = 0; j < abstandInt; j++) {
                        if (ausgabe.length() > 9){
                            break;
                        }else{
                            ausgabe.append("O");
                        }
                    }
                    if (y < randomZahl) {
                        if (ausgabe.length() > 8){
                            break;
                        }else{
                            ausgabe.append("X");
                        }
                    }
                }
            }
            if (randomZahl + 1 >= 3){
                ausgabe.deleteCharAt(9);
            }
            System.out.println(ausgabe);
        }
    }
}
