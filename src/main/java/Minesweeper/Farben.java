package Minesweeper;

/**
 * Eine Enumeration von Farben um die Konsolenausgabe zu gestallten.
 * Quelle: https://dev.to/awwsmm/coloured-terminal-output-with-java-57l3
 */
public enum Farben {
    Schwarz("\u001B[30m"),
    Rot("\u001B[31m"),
    Gruen("\u001B[32m"),
    Gelb("\u001B[33m"),
    Blau("\u001B[34m"),
    Violet("\u001B[35m"),
    Cyan("\u001B[36m"),
    Weiss("\u001B[37m");

    private String farbe;

    Farben(String s) {
        farbe = s;
    }

    String erhalteFarbe(){
        return farbe;
    }
}
