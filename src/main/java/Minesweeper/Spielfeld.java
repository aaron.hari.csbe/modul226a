package Minesweeper;

import java.util.Random;

/**
 * Enthält alle Zellens
 */
public class Spielfeld {
    int HOEHE = 16;
    int BREITE = 16;
    int ANZAHL_MINEN = 40;

    final char MARKIERUNG = 'P';
    final char VERDECKT = '#';

    Zelle[][] spielfeld;

    /**
     * Verteillt die ANZAHL_MINEN auf einem Spilfeld.
     */
    void zufaelligMinenVerteilen() {
        for (int i = 0; i < ANZAHL_MINEN; i++) {
            boolean istMine = true;
            while (istMine){
                int mineKoordinateX = zufaelligeZahl(0, BREITE);
                int mineKoordinateY = zufaelligeZahl(0, HOEHE);
                if (!spielfeld[mineKoordinateX][mineKoordinateY].IST_MINE) {
                    istMine = false;
                    spielfeld[mineKoordinateX][mineKoordinateY].IST_MINE = true;
                }
            }

        }
    }

    /**
     * Erstellt ein neues Spielfeld und befüllt es mit Zellen.
     */
    void spielfeldErstellen(){
         spielfeld = new Zelle[BREITE][HOEHE];
        for(int i = 0; i < BREITE; i++){
            for (int j = 0; j < HOEHE; j++){
                spielfeld[i][j] = new Zelle();
            }
        }
    }

    /**
     * Geht jede einzelne Zelle durch und berechnet für jede einzelne Zelle die anzahl Minen im Umkreis soferd es sich um keine Mine handelt.
     */
    void anzahlMinenInUmkreisBerechnen(){
        for(int i = 0; i < BREITE; i++) {
            for (int j = 0; j < HOEHE; j++) {
                if (!spielfeld[i][j].istMine()){
                    for (int x = i - 1; x <= i + 1; x++){
                        for (int y = j - 1; y <= j + 1; y++){
                            if (x >= 0 && x < BREITE && y >= 0 && y < HOEHE){
                                if (spielfeld[x][y].istMine()){
                                    spielfeld[i][j].ANZAHL_MINEN_IN_UMKREIS++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void spielfeldAnzeigen(){
        System.out.print("    ");
        for (int y = 0; y < BREITE; y++){
            System.out.print(Farben.Blau.erhalteFarbe() + (y + 1));
            int anzahlZiffern = (anzahlZiffern(y + 1) * -1) + 3;
            for (int a = 0; a < anzahlZiffern; a++){
                System.out.print(" ");
            }
        }
        System.out.println("");
        for (int i = 0; i < HOEHE; i++){
            System.out.print(Farben.Blau.erhalteFarbe() + (i + 1));
            int anzahlZiffern = (anzahlZiffern(i + 1) * -1) + 4;
            for (int a = 0; a < anzahlZiffern; a++){
                System.out.print(" ");
            }
            for (int j = 0; j < BREITE; j++){
                if (spielfeld[j][i].IST_VERDECKT && spielfeld[j][i].IST_MARKIERT){
                    System.out.print(Farben.Weiss.erhalteFarbe() + MARKIERUNG);
                }else if (spielfeld[j][i].IST_VERDECKT){
                    System.out.print(Farben.Weiss.erhalteFarbe() + VERDECKT);
                }
                System.out.print("  ");
            }
            System.out.println("");
        }
    }

    int anzahlZiffern(int zahl){
        int anzahlZiffern = 1;

        while (zahl / Math.pow(10, anzahlZiffern) >= 1) {
            anzahlZiffern++;
        }
        return anzahlZiffern;
    }


    /**
     * Generiert eine zueföllige Zahl inerhlalb der angegebenen maximum und minimum Grenze.
     *
     * @param kleinsteMoeglicheZahl bestimmt die kleinst mögliche Zahl welche generiert werden kann.
     * @param groesstMoeglicheZahl  bestimmt die grösst mögliche Zahl welche generiert werden kann.
     * @return gibt die zuföllige Zahl als int zurück.
     */
    int zufaelligeZahl(int kleinsteMoeglicheZahl, int groesstMoeglicheZahl) {
        if (kleinsteMoeglicheZahl > groesstMoeglicheZahl) {
            return 0;
        }
        Random generator = new Random();
        int differenz = groesstMoeglicheZahl - kleinsteMoeglicheZahl;
        int zufaelligeZahl = generator.nextInt(differenz);
        zufaelligeZahl += kleinsteMoeglicheZahl;
        return zufaelligeZahl;
    }
}
