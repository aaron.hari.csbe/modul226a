package Minesweeper;

/**
 * Beschreibt den Typ der Zelle und ob sie Ver- oder Aufgedeckt ist.
 */
public class Zelle {
    boolean IST_VERDECKT = true;
    boolean IST_MARKIERT = false;
    boolean IST_MINE = false;
    short ANZAHL_MINEN_IN_UMKREIS = 0;


    /**
     *
     * @return gibt den Wert von IST_MINE als boolean zurück.
     */
    boolean istMine(){
        return IST_MINE;
    }

    /**
     *
     * @return gibt den Wert von IST_VERDECKT als boolean zurück.
     */
    boolean istVerdeckt(){
        return IST_VERDECKT;
    }

    /**
     *
     * @return gibt den Wert von IST_MARKIERT als boolean zurück.
     */
    boolean istMarkiert(){
        return IST_MARKIERT;
    }

    /**
     *
     * @return gibt den Wert von ANZAHL_MINIEN_IN_UMKREIS als short zurück.
     */
    short anzahlMinenInUmkreis(){
        return ANZAHL_MINEN_IN_UMKREIS;
    }

    /**
     * Deckt eine Zelle auf.
     */
    void aufdecken(){
        IST_VERDECKT = false;
    }

    /**
     * Markiert eine Zelle.
     */
    void markieren(){
        IST_MARKIERT = true;
    }

    /**
     * Hebt die Markierung einer Markierten Zelle auf.
     */
    void markierungAufheben(){
        IST_MARKIERT = false;
    }
}
